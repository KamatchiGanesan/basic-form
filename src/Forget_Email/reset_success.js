import React, { Component } from "react";
import "./forget.css";
import { Link } from 'react-router-dom'

class Email extends Component {
  constructor() {
    super();
    this.state = {

    };
    this.handleClick = this.handleClick.bind(this);
    this.showPass = this.showPass.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSwitch = this.handleSwitch.bind(this);
  }
  handleSwitch(){
    this.setState(state => ({
      switch_otp_pass: !state.switch_otp_pass
    }));

  }
  handleChange(event){
    this.setState({[event.target.name] : event.target.value})
    if(event.target.value === ""){
      this.setState({[event.target.name+"_error"]:"please fill the Value"})
    }else{
      this.setState({[event.target.name+"_error"]:""}) 
    }
  }
  handleClick() {
    this.setState(state => ({
      showPass: !state.showPass,
      switch_uesr_pass: !state.switch_uesr_pass
    }));
  }

  showPass() {
    this.setState(state => ({
      showPass: !state.showPass
    }));
  }

  
  render() {
    return (
      <div>
        <div className="forget">
          <div className="login-page dark login-page-background">
            <div className="scroll-login">
              <div className="login-card">
                
                <div className="login-card_section">
                  <div className="card-body-section-center text-center">
                    <div className="center-flex">
                      <div className="login-card-header no-pad">
                        <div>
                          <h2>Password reset successful  </h2>
                          <small>You have successfully reset your password. Please use your new password when logging in. </small>
                        </div>
                      </div>
                      <div className=" no-pad">
                        <div className="success-icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
	 viewBox="0 0 512 512" ><g><g><g><path d="M383.841,171.838c-7.881-8.31-21.02-8.676-29.343-0.775L221.987,296.732l-63.204-64.893
   c-8.005-8.213-21.13-8.393-29.35-0.387c-8.213,7.998-8.386,21.137-0.388,29.35l77.492,79.561
   c4.061,4.172,9.458,6.275,14.869,6.275c5.134,0,10.268-1.896,14.288-5.694l147.373-139.762
   C391.383,193.294,391.735,180.155,383.841,171.838z"/><path d="M256,0C114.84,0,0,114.84,0,256s114.84,256,256,256s256-114.84,256-256S397.16,0,256,0z M256,470.487
        c-118.265,0-214.487-96.214-214.487-214.487c0-118.265,96.221-214.487,214.487-214.487c118.272,0,214.487,96.221,214.487,214.487
        C470.487,374.272,374.272,470.487,256,470.487z"/></g></g></g></svg>
</div>
                            <div className="row">
                            <div className="col-md-12 col-xs-12 text-center">
                                <Link to="/login" className="gradeinttext signin">Login</Link>
                              </div>
                             
                            </div>
                       
                         
                       
                        
                      </div>
                      <div className="login-card-footer"></div>
                    </div>
                  </div>
                </div>
               

                <div className="footer-mobile">
                  Licensed By &#169; aalavai.com . All rights reserved.
                </div>
              </div>
            </div>{" "}
            <div className="footer">
              Licensed By &#169; aalavai.com . All rights reserved.
            </div>{" "}
          </div>
        </div>{" "}
      </div>
    );
  }
}

export default Email;
