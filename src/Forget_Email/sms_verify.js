import React, { Component } from "react";
import "./forget.css";
import { Link } from 'react-router-dom';
import "./otp_main.js";
import axios from 'axios';
import Config from '../Config';
import { Redirect } from 'react-router'

class Email extends Component {
  constructor(props) {

    super(props);
    this.state = {
        error: '',
        serverErrors: {},
        fields: {},
        loaderStatus: false,
        email_id: props.location.state.email_id,
    };


    this.handleClick = this.handleClick.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
  }
  handleSwitch(){
    this.setState(state => ({
      switch_otp_pass: !state.switch_otp_pass
    }));
  }
    
    /**
     * setOTP Set OTP
     */
    setOTP(){
        return parseInt(
                this.state.fields["opt1"]+
                this.state.fields["opt2"]+
                this.state.fields["opt3"]+
                this.state.fields["opt4"]
            );
    }

    /**
     * handleClick Save otp form
     * 
     * @return mixed 
     */
    handleClick() {
        
        if(this.validateOTP()){
            this.setState({loaderStatus: true})
            let params = {
                    otp:      this.setOTP(),
                    username: this.state.email_id
            };

            axios.post(Config[process.env.NODE_ENV + '_API_URL'] + '/react/verify-otp', params)
            .then(res => {
                this.setState({loaderStatus: false})
                    
                window.location = '/login';
            }).catch((error)=>{
                if(error.response){
                    this.setState({loaderStatus: false});
                    this.setState({serverErrors: error.response.data.errors});    
                }
                
            })
        } 
    }
    
    /**
     * handleOnChange handle change
     * 
     * @param  mixed event Event
     * 
     * @return mixed Event
     */
    handleOnChange(event){
        
        this.setState({
            error: '',
            serverErrors: {}
        });

        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        
         const form = event.target.form;
        const index = Array.prototype.indexOf.call(form, event.target);
        form.elements[index + 1].focus();

        this.setState({
            fields
        });
    }

   /**
   * validateError Validate Error
   * 
   * @return boolean isValid
   */
    validateOTP() {

        let fields = this.state.fields;
        
        let error = '';
        let formIsValid = true;

        if (!fields['opt1']) {
          formIsValid = false;
          error = "*Please enter OTP";
        }

        if (!fields['opt2']) {
          formIsValid = false;
          error = "*Please enter OTP";
        }

        if (!fields['opt3']) {
          formIsValid = false;
          error = "*Please enter OTP";
        }

        if (!fields['opt4']) {
          formIsValid = false;
          error = "*Please enter valid OTP";
        }


        if(error){
            this.setState({
              error: error
            });    
        }

        return formIsValid;
    }
  

  render() {
    return (
      <div>
        <div className="forget">
          <div className="login-page dark login-page-background">
            <div className="scroll-login">
              <div className="login-card">
                <div className={this.state.loaderStatus ? "loader display-block":"loader display-none"}>
                  <div className="loaderBar"></div>
                </div>
                <div className="login-card_section">
                  <div className="card-body-section-center">
                    <div className="center-flex">
                      <div className="login-card-header no-pad">
                        <div>
                          <h2>Enter 4 digit recovery code  </h2>
                          <small>The recovery code was sent to your mobile number </small>
                        </div>
                      </div>
                      <div className=" no-pad">
                        <form autoComplete="off">
                        <label>Please enter the code</label>
                        <div class ="login-form otp">
                  <div className="input-group">
                  <div className=" input-effect">
            <input className="effect-16" id="OTP1" placeholder="-" maxlength="1" name="opt1" type="text" placeholder="" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)" value={this.state.fields.otp1} onChange={this.handleOnChange}/>
          
            <span className="focus-border"></span>
        </div></div>
        <div className="input-group">
          <div className=" input-effect">
    <input className="effect-16" id="OTP2" placeholder="-" maxlength="1" name="opt2" type="text" placeholder="" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(1)" value={this.state.fields.otp2} onChange={this.handleOnChange}/>
   
    <span className="focus-border"></span>
</div></div>
<div className="input-group">
  <div className=" input-effect">
<input className="effect-16" id="OTP3" placeholder="-" maxlength="1" name="opt3" type="text" placeholder="" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(1)" value={this.state.fields.otp3} onChange={this.handleOnChange}/>

<span className="focus-border"></span>
</div></div>
<div className="input-group">
  <div className=" input-effect">
<input className="effect-16" id="OTP4" placeholder="-" maxlength="1"  name="opt4" type="text" placeholder="" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(1)" value={this.state.fields.otp4} onChange={this.handleOnChange}/>

<span className="focus-border"></span>
</div></div>
<div className="errorMsg">{this.state.error}</div>
<div className="errorMsg">{this.state.serverErrors?.otp}</div>
        
                  </div>
                  <div className="row">
                            <div className="col-md-12 col-xs-12 text-right">
                                <button
                                  type="button"
                                  className="gradeinttext signin"
                                  onClick={this.handleClick}
                                >
                                  Next
                                </button>
                              </div>
                              <div className="col-md-12 col-xs-12 text-left">
                                <Link className="link forgetpassword-btn">
                                  Sign In 
                                </Link>
                              </div>
                             <div className="col-md-12 col-xs-12 text-left mt-10">
                                <span>Do not have an account? </span>
                                <Link to="/signup" className="link signup-btn"> Sign Up</Link>
                              </div>
                            </div>
                       
                         
                        </form>
                        
                      </div>
                      <div className="login-card-footer"></div>
                    </div>
                  </div>
                </div>
                

                <div className="footer-mobile">
                  Licensed By &#169; aalavai.com . All rights reserved.
                </div>
              </div>
            </div>{" "}
            <div className="footer">
              Licensed By &#169; aalavai.com . All rights reserved.
            </div>{" "}
          </div>
        </div>{" "}
      </div>
    );
  }
}

export default Email;
