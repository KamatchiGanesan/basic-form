import React, { Component } from "react";
import "./forget.css";
import { Link } from 'react-router-dom'
import axios from 'axios';
import Config from '../Config';

class Sms extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {},
      username_content:false,
      message: '',
      loaderStatus: false,
      serverErrors: {},
    };
    this.handleClick = this.handleClick.bind(this);
    this.showPass = this.showPass.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleSwitch = this.handleSwitch.bind(this);
    this.handleResetPwdForm = this.handleResetPwdForm.bind(this);
  }
  handleSwitch(){
    this.setState(state => ({
      switch_otp_pass: !state.switch_otp_pass
    }));

  }
  handleOnChange(event){
    let fields = this.state.fields;
    fields[event.target.name] = event.target.value;
    this.setState({
      fields
    });
    if(event.target.value===""){
      this.setState({[event.target.name+"_content"]:false})
    }
    else{
      this.setState({[event.target.name+"_content"]:true})
    }
  }
  handleClick() {
    this.setState(state => ({
      showPass: !state.showPass,
      switch_uesr_pass: !state.switch_uesr_pass
    }));
  }
  showPass() {
    this.setState(state => ({
      showPass: !state.showPass
    }));
  }
  handleResetPwdForm(event){
    event.preventDefault();
    if (this.validateForm()) {
        this.setState({loaderStatus: true})
        this.state.fields["type"] = 2;
        
        let params = {
            type: this.state.fields["type"],
            phone_number: this.state.fields["username"]
        };  

        axios.post(Config[process.env.NODE_ENV + '_API_URL'] + '/react/forgot-password-phone', params)
        .then(res => {
        if(res.status == 200){

          this.setState({loaderStatus: false})

          this.props.history.push({
                pathname: '/phoneverifyotp',
                state: { email_id: res.data.email_id, type: 1, label: 'Recovery Code'}
            });
        }
      }).catch((error)=>{
        this.setState({loaderStatus: false})
        this.setState({serverErrors: error.response.data.errors});
      })
    }
  }
  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["username"]) {
      formIsValid = false;
      errors["username"] = "*Please enter your mobile no.";
    }

    if (typeof fields["username"] !== "undefined") {
      if (!fields["username"].match(/^[0-9]{10}$/)) {
        formIsValid = false;
        errors["username"] = "*Please enter valid mobile no.";
      }
    }
    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  render() {
    return (
      <div>
        <h4 className="response-message">{this.state.message}</h4>
        <div className="forget">
          <div className="login-page dark login-page-background">
            <div className="scroll-login">
              <div className="login-card">
                <div className={this.state.loaderStatus ? "loader display-block":"loader display-none"}>
                  <div class="loaderBar"></div>
                </div>
                <div className="login-card_section">
                  <div className="card-body-section-center">
                    <div className="center-flex">
                      <div className="login-card-header no-pad">
                        <div>
                          <h2>Forget  </h2>
                          <small>Please Click any one below to Recover your account </small>
                        </div>
                      </div>
                      <div className=" no-pad">
                        <form autoComplete="off" onSubmit={this.handleResetPwdForm}>
                   
                            <div className="input-section sections-step step2">
                              <div className="input-group">
                                <div className=" input-effect">
                                  <input
                                    className={this.state.username_content ? "effect-16 has-content":"effect-16"}
                                    id="username"
                                    name="username"
                                    type="text"
                                    placeholder=""
                                    value={this.state.fields.username} 
                                    onChange={this.handleOnChange}
                                  />
                                  <label>
                                    Phone No
                                    <span className="required">*</span>
                                    <span className="error"></span>
                                  </label>
                                  <div className="abs-btn-icons">
                                    {" "}
                                    <i className="clear">
                                      <svg
                                        height="511.992pt"
                                        viewBox="0 0 511.992 511.992"
                                        width="511.992pt"
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <path
                                          d="m415.402344 495.421875-159.40625-159.410156-159.40625 159.410156c-22.097656 22.09375-57.921875 22.09375-80.019532 0-22.09375-22.097656-22.09375-57.921875 0-80.019531l159.410157-159.40625-159.410157-159.40625c-22.09375-22.097656-22.09375-57.921875 0-80.019532 22.097657-22.09375 57.921876-22.09375 80.019532 0l159.40625 159.410157 159.40625-159.410157c22.097656-22.09375 57.921875-22.09375 80.019531 0 22.09375 22.097657 22.09375 57.921876 0 80.019532l-159.410156 159.40625 159.410156 159.40625c22.09375 22.097656 22.09375 57.921875 0 80.019531-22.097656 22.09375-57.921875 22.09375-80.019531 0zm0 0"
                                          fill="#000"
                                        />
                                      </svg>
                                    </i>
                                  </div>

                                  <span className="focus-border"></span>
                                </div>
                                <span className="error">
                                  <span className="info-icon">
                                    <svg
                                      version="1.1"
                                      id="Capa_1"
                                      xmlns="http://www.w3.org/2000/svg"
                                      x="0px"
                                      y="0px"
                                      viewBox="0 0 512 512"
                                    >
                                      <g>
                                        <g>
                                          <path
                                            d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M298.667,416
    c0,5.896-4.771,10.667-10.667,10.667h-64c-5.896,0-10.667-4.771-10.667-10.667V256h-10.667c-5.896,0-10.667-4.771-10.667-10.667
    v-42.667c0-5.896,4.771-10.667,10.667-10.667H288c5.896,0,10.667,4.771,10.667,10.667V416z M256,170.667
    c-23.531,0-42.667-19.135-42.667-42.667S232.469,85.333,256,85.333s42.667,19.135,42.667,42.667S279.531,170.667,256,170.667z"
                                          />
                                        </g>
                                      </g>
                                    </svg>
                                  </span>
                                  {/* {this.state.username_error} */}
                                </span>
                              </div>
                              <div className="errorMsg">{this.state.serverErrors?.phone_number}</div>
                              <div className="errorMsg">{this.state.errors.username}</div>
                            </div>
                            <div className="row">
                            <div className="col-md-12 col-xs-12 text-right">
                                <button
                                  type="submit"
                                  className="gradeinttext signin"
                                  onClick={this.handleClick}
                                >
                                  Next
                                </button>
                              </div>
                              <div className="col-md-12 col-xs-12 text-left">
                                <Link className="link forgetpassword-btn" to="/login">
                                  Sign In 
                                </Link>
                              </div>
                             <div className="col-md-12 col-xs-12 text-left mt-10">
                                <span>Do not have an account? </span>
                                <Link to="/signup" className="link signup-btn"> Sign Up</Link>
                              </div>
                            </div>
                       
                         
                        </form>
                        
                      </div>
                      <div className="login-card-footer"></div>
                    </div>
                  </div>
                </div>
             

                <div className="footer-mobile">
                  Licensed By &#169; aalavai.com . All rights reserved.
                </div>
              </div>
            </div>{" "}
            <div className="footer">
              Licensed By &#169; aalavai.com . All rights reserved.
            </div>{" "}
          </div>
        </div>{" "}
      </div>
    );
  }
}

export default Sms;
