var config = [];

//Development Settings will go here
config['development_API_URL'] = 'http://127.0.0.1:8000/api';
config['development_FRONT_URL'] = 'http://localhost:8081';
config['development_API_HEADERS'] = {
	'Content-Type': 'application/json'
};

export default config;