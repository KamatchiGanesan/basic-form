import React, { Component } from "react";
import "../Login/login.css";
import axios from 'axios';
import { Link } from "react-router-dom";
import Config from '../Config';
import { Redirect } from 'react-router'

class SignupPage extends Component {

  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {},
      serverErrors: {},
      first_name_content:false,
      last_name_content:false,
      email_id_content:false,
      phone_number_content:false,
      confirm_password_content:false,
      password_content:false,
      message: '',
      loaderStatus: false,
    };
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleSignupForm = this.handleSignupForm.bind(this);
  }
  handleClick() {
    this.setState(state => ({
      showPass: !state.showPass,
      switch_uesr_pass: !state.switch_uesr_pass
    }));
  }
  showPass() {
    this.setState(state => ({
      showPass: !state.showPass
    }));
  }
  handleOnChange(event){
    let fields = this.state.fields;
    fields[event.target.name] = event.target.value;
    this.setState({
      fields
    });
    if(event.target.value===""){
      this.setState({[event.target.name+"_content"]:false})
    }
    else{
      this.setState({[event.target.name+"_content"]:true})
    }
  }
  handleSignupForm(event){
    event.preventDefault();

    if (this.validateForm()) {

      this.setState({loaderStatus: true})
      this.state.fields["role_id"] = 1;
      
      axios.post(Config[process.env.NODE_ENV + '_API_URL'] + '/react/signin/store', this.state.fields)
      .then(res => {
        if(res.status == 200){
            this.setState({loaderStatus: false})
            this.setState({message: res.data.message})
            
            this.props.history.push({
            pathname: '/smsverify',
                state: { email_id: this.state.fields["email_id"] , label: 'Verification Code'}
            });
          
        }
      }).catch((error)=>{
        this.setState({loaderStatus: false});
        this.setState({serverErrors: error.response.data.errors});
      })
    }
  }
  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["first_name"]) {
      formIsValid = false;
      errors["first_name"] = "*Please enter your firstname.";
    }
    if (!fields["last_name"]) {
      formIsValid = false;
      errors["last_name"] = "*Please enter your lastname.";
    }

    if (typeof fields["first_name"] !== "undefined") {
      if (!fields["first_name"].match(/^[a-zA-Z ]*$/)) {
        formIsValid = false;
        errors["first_name"] = "*Please enter alphabet characters only.";
      }
    }

    if (typeof fields["last_name"] !== "undefined") {
      if (!fields["last_name"].match(/^[a-zA-Z ]*$/)) {
        formIsValid = false;
        errors["last_name"] = "*Please enter alphabet characters only.";
      }
    }

    if (!fields["email_id"]) {
      formIsValid = false;
      errors["email_id"] = "*Please enter your email-ID.";
    }

    if (typeof fields["email_id"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(fields["email_id"])) {
        formIsValid = false;
        errors["email_id"] = "*Please enter valid email-ID.";
      }
    }

    if (!fields["phone_number"]) {
      formIsValid = false;
      errors["phone_number"] = "*Please enter your mobile no.";
    }

    if (typeof fields["phone_number"] !== "undefined") {
      if (!fields["phone_number"].match(/^[0-9]{10}$/)) {
        formIsValid = false;
        errors["phone_number"] = "*Please enter valid mobile no.";
      }
    }

    if (!fields["confirm_password"]) {
      formIsValid = false;
      errors["confirm_password"] = "*Please enter your confirm password.";
    }

    if (typeof fields["confirm_password"] !== "undefined") {
      if (!fields["confirm_password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
        formIsValid = false;
        errors["confirm_password"] = "*Please enter secure and strong password.";
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "*Please enter your password.";
    }

    if (typeof fields["password"] !== "undefined") {
      if (!fields["password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
        formIsValid = false;
        errors["password"] = "*Please enter secure and strong password.";
      }
    }

    this.setState({
      errors: errors
    });
    
    return formIsValid;
  }

  
  render() {
    return (
      <div>
        <h4 className="response-message">{this.state.message}</h4>
        <div className="sign-up">
          <div className="login-page dark login-page-background">
            <div className="scroll-login">
              <div className="login-card">
                <div className={this.state.loaderStatus ? "loader display-block":"loader display-none"}>
                  <div className="loaderBar"></div>
                </div>
                <div className="login-card_section_two">
                  <div className="card-body-section-center">
                    <div className="center-flex">
                      <div className="login-card-header no-pad">
                        <div>
                          <h2>Create your explorer account</h2>
                          <small> to continue to Explore</small>
                        </div>
                      </div>
                      <div className="login-card-body no-pad">
                        <form onSubmit={this.handleSignupForm}>
                          <div className="login-form user-section">
                            <div className="user-info">
                              <div className="row">
                                <div className="col-md-6 col-xs-12">
                                  <div className="input-group">
                                    <div className=" input-effect">
                                      <input
                                        className={this.state.first_name_content ? "effect-16 has-content":"effect-16"}
                                        id="first_name"
                                        name="first_name"
                                        type="text"
                                        placeholder=""
                                        value={this.state.fields.first_name} 
                                        onChange={this.handleOnChange}
                                      />
                                      <label>
                                        First Name
                                        <span className="required">*</span>
                                        <span className="error"></span>
                                      </label>
                                      <div className="abs-btn-icons"></div>

                                      <span className="focus-border"></span>
                                    </div>
                                  </div>
                                  <div className="errorMsg">{this.state.errors.first_name}</div>
                                </div>
                                <div className="col-md-6 col-xs-12">
                                  <div className="input-group">
                                    <div className=" input-effect">
                                      <input
                                        className={this.state.last_name_content ? "effect-16 has-content":"effect-16"}
                                        id="last_name"
                                        name="last_name"
                                        type="text"
                                        placeholder=""
                                        value={this.state.fields.last_name}
                                        onChange={this.handleOnChange}
                                      />
                                      <label>
                                        Last Name
                                        <span className="required">*</span>
                                        <span className="error"></span>
                                      </label>
                                      <div className="abs-btn-icons"></div>

                                      <span className="focus-border"></span>
                                    </div>
                                  </div>
                                  <div className="errorMsg">{this.state.errors.last_name}</div>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-12">
                                  {" "}
                                  <div className="input-group">
                                    <div className=" input-effect">
                                      <input
                                        className={this.state.email_id_content ? "effect-16 has-content":"effect-16"}
                                        id="email_id"
                                        name="email_id"
                                        type="text"
                                        placeholder=""
                                        value={this.state.fields.email_id}
                                        onChange={this.handleOnChange}
                                      />
                                      <label>
                                        Email-id{" "}
                                        <span className="required">*</span>
                                        <span className="error"></span>
                                      </label>
                                      <div className="abs-btn-icons"></div>

                                      <span className="focus-border"></span>
                                    </div>
                                  </div>
                                  <div className="errorMsg">{this.state.serverErrors?.email_id}</div>
                                  <div className="errorMsg">{this.state.errors.email_id}</div>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-12">
                                  <div className="input-group">
                                    <div className=" input-effect">
                                      <input
                                        className={this.state.phone_number_content ? "effect-16 has-content":"effect-16"}
                                        id="phone_number"
                                        name="phone_number"
                                        type="text"
                                        value={this.state.fields.phone_number}
                                        placeholder=""
                                        onChange={this.handleOnChange}
                                      />
                                      <label>
                                        Phone No
                                        <span className="required">*</span>
                                        <span className="error"></span>
                                      </label>
                                      <div className="abs-btn-icons"></div>

                                      <span className="focus-border"></span>
                                    </div>
                                  </div>
                                  <div className="errorMsg">{this.state.serverErrors?.phone_number}</div>
                                  <div className="errorMsg">{this.state.errors.phone_number}</div>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-6">
                                  <div className="input-group">
                                    <div className=" input-effect">
                                      <input
                                        className={this.state.confirm_password_content ? "effect-16 has-content":"effect-16"}
                                        id="confirm_password"
                                        name="confirm_password"
                                        type="password"
                                        placeholder=""
                                        value={this.state.fields.confirm_password}
                                        onChange={this.handleOnChange}
                                      />
                                      <label>
                                        Confirm Password
                                        <span className="required">*</span>
                                        <span className="error"></span>
                                      </label>
                                      <div className="abs-btn-icons"></div>

                                      <span className="focus-border"></span>
                                    </div>
                                  </div>
                                  <div className="errorMsg">{this.state.errors.confirm_password}</div>
                                </div>
                                <div className="col-md-6">
                                  <div className="input-group">
                                    <div className=" input-effect">
                                      <input
                                        className={this.state.password_content ? "effect-16 has-content":"effect-16"}
                                        id="password"
                                        name="password"
                                        type="password"
                                        placeholder=""
                                        value={this.state.fields.password}
                                        onChange={this.handleOnChange}
                                      />
                                      <label>
                                        New Password
                                        <span className="required">*</span>
                                        <span className="error"></span>
                                      </label>
                                      <div className="abs-btn-icons"></div>

                                      <span className="focus-border"></span>
                                    </div>
                                  </div>
                                  <div className="errorMsg">{this.state.errors.password}</div>
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md-12 col-xs-12 text-right">
                                <button
                                  type="submit"
                                  className="gradeinttext signin"
                                >
                                  Create
                                </button>
                              </div>

                              <div className="col-md-12 col-xs-12 text-right mt-10">
                                <span>have an account? Please Click to go</span>
                                <a className="link signup-btn"><Link to='/login'>Sign In</Link></a>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="login-card-footer"></div>
                    </div>
                  </div>
                </div>
                <div className="login-card_section_one text-center">
                  <div className="card-body-section-center">
                    <center>
                      <img src="" className="card-logo" />
                    </center>
                    <h1>Title</h1>
                    <p>
                      {" "}
                      Welcome to <b> CloudEnsure</b> Monitor your AWS Resources,
                      Best Practices Compliances & Optimize your spend.
                    </p>
                  </div>
                </div>

                <div className="footer-mobile">
                  Licensed By &#169; aalavai.com . All rights reserved.
                </div>
              </div>
            </div>{" "}
            <div className="footer">
              Licensed By &#169; aalavai.com . All rights reserved.
            </div>{" "}
          </div>
        </div>{" "}
      </div>
    );
  }
}

export default SignupPage;
