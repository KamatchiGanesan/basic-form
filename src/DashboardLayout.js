
    import React, { Component } from 'react';  
    import { Route } from 'react-router-dom';  
    import Header from './layout_files/header';
    import PerfectScrollbar from 'react-perfect-scrollbar';
      
    const DashboardLayout = ({children, ...rest}) => {  
      return (  
        <div className="page page-dashboard">  
          {children}  
        </div>  
      )  
    }  
      
    const DashboardLayoutRoute = ({component: Component, ...rest}) => {  
      return (  
        <Route {...rest} render={matchProps => (  
          <DashboardLayout>  
             <div>
     <div className="page-container wout-side">
       <Header></Header>
      <main className='main-content bgc-grey-100'>
        <div id='mainContent'>
        <div className="notification_toast" >
           </div>
          <div className="full-container">
          <PerfectScrollbar  onScrollY={container => console.log(`scrolled to: ${container.scrollTop}.`)} onScrollDown={container => console.log(`Down scrolled to: ${container.scrollHeight-container.offsetHeight}.`)}>
          <Component {...matchProps} />
          </PerfectScrollbar>
          </div>
          
        </div>
      </main>

    
      <footer className="bdT ta-r lh-0 fsz-sm c-grey-600 hide">
        <span className="breadcrumbs_container"><nav aria-label="breadcrumb">
<ol className="breadcrumb">
  <li className="breadcrumb-item"><a href="#">Home</a></li>
  <li className="breadcrumb-item"><a href="#">Live Agent</a></li>
  <li className="breadcrumb-item" aria-current="page">Chat window</li>
</ol>
</nav></span><span> Licensed By : PUC <a href="#" target='_blank' title="Colorlib"> Botzer Management Platform </a>. All rights reserved.</span>
      </footer>
      
    </div>
    </div>
              
          </DashboardLayout>  
        )} />  
      )  
    };  
      
    export default DashboardLayoutRoute;  
