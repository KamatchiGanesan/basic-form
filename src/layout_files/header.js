import React,{ Component} from 'react';




class header extends Component{

  constructor(props) {

    super(props);
    this.addActiveClass= this.addActiveClass.bind(this);
    this.addSidebarClass= this.addSidebarClass.bind(this);
    this.addProfileClass= this.addProfileClass.bind(this);
    this.state = {
      isActive: false,
      isSidebar: false,
      isProfile: false

    }

  }
  addActiveClass() {
    this.setState({
      isActive: !this.state.isActive
    })
  }
  addSidebarClass() {
    this.setState({
      isSidebar: !this.state.isSidebar
    })
  }
  addProfileClass() {
    this.setState({
      isProfile: !this.state.isProfile
    })
  }
   
    
    
      
render(){
    return(
        <div className="header navbar">
          <div className="header-container container">
          <div className="row desktop-header" >
    <div className="col-md-2 text-left"> <ul className="nav-left">
              <li>
                <a id='logo' className="sidebar-toggle" href="" >
                  LOGO
                </a>
              </li>
              <li className="header-search">
              
              </li>
            </ul></div>
    <div className="col-md-6"><form autoComplete="off" action="/action_page.php">
  <div className="input-group md-form form-sm form-2 input-group md-form form-sm form-2 pl-0 ml-0 mt-0 mb-0 search-explore">
<input id="myInput" name="myCountry" className="form-control my-0 py-1 red-border search-input" type="text" placeholder="Search" aria-label="Search"/>
<div className="input-group-append">
  <span className="input-group-text search-btn" id="basic-text1"><i className="fas fa-search text-grey" aria-hidden="true"></i></span>
</div>
</div>
</form></div> 
    <div className="col-md-4 text-right"> <ul className="nav-right">
              <li className="notifications dropdown">
                <span className="counter bgc-red">3</span>
                <a href="" className="dropdown-toggle no-after" data-toggle="dropdown">
                  <i className="ti-bell"></i>
                </a>

                <ul className="dropdown-menu">
                  <li className="pX-20 pY-15 bdB">
                    <i className="ti-bell pR-10"></i>
                    <span className="fsz-sm fw-600 c-grey-900">Notifications</span>
                  </li>
                  <li>
                    <ul className="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <span>
                              <span className="fw-500">Prem Kumar</span>
                              <span className="c-grey-600">liked your <span className="text-dark">post</span>
                              </span>
                            </span>
                            <p className="m-0">
                              <small className="fsz-xs">5 mins ago</small>
                            </p>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <span>
                              <span className="fw-500">Bakrudeen</span>
                              <span className="c-grey-600">liked your <span className="text-dark">cover image</span>
                              </span>
                            </span>
                            <p className="m-0">
                              <small className="fsz-xs">7 mins ago</small>
                            </p>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <span>
                              <span className="fw-500">Kesavan </span>
                              <span className="c-grey-600">commented on your <span className="text-dark">video</span>
                              </span>
                            </span>
                            <p className="m-0">
                              <small className="fsz-xs">10 mins ago</small>
                            </p>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="pX-20 pY-15 ta-c bdT">
                    <span>
                      <a href="" className="c-grey-600 cH-blue fsz-sm td-n">View All Notifications <i className="ti-angle-right fsz-xs mL-10"></i></a>
                    </span>
                  </li>
                </ul>
              </li>
              <li className="notifications dropdown">
                <span className="counter bgc-blue">3</span>
                <a href="" className="dropdown-toggle no-after" data-toggle="dropdown">
                  <i className="ti-headphone-alt"></i>
                </a>

                <ul className="dropdown-menu">
                  <li className="pX-20 pY-15 bdB">
                    <i className="ti-email pR-10"></i>
                    <span className="fsz-sm fw-600 c-grey-900">Emails</span>
                  </li>
                  <li>
                    <ul className="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <div>
                              <div className="peers jc-sb fxw-nw mB-5">
                                <div className="peer">
                                  <p className="fw-500 mB-0">Premkumar</p>
                                </div>
                                <div className="peer">
                                  <small className="fsz-xs">5 mins ago</small>
                                </div>
                              </div>
                              <span className="c-grey-600 fsz-sm">
                                Want to create your own customized data generator for your app...
                              </span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <div>
                              <div className="peers jc-sb fxw-nw mB-5">
                                <div className="peer">
                                  <p className="fw-500 mB-0">Shyam </p>
                                </div>
                                <div className="peer">
                                  <small className="fsz-xs">15 mins ago</small>
                                </div>
                              </div>
                              <span className="c-grey-600 fsz-sm">
                                Want to create your own customized data generator for your app...
                              </span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <div>
                              <div className="peers jc-sb fxw-nw mB-5">
                                <div className="peer">
                                  <p className="fw-500 mB-0">Bala</p>
                                </div>
                                <div className="peer">
                                  <small className="fsz-xs">25 mins ago</small>
                                </div>
                              </div>
                              <span className="c-grey-600 fsz-sm">
                                Want to create your own customized data generator for your app...
                              </span>
                            </div>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="pX-20 pY-15 ta-c bdT">
                    <span>
                      <a href="email.html" className="c-grey-600 cH-blue fsz-sm td-n">View All Email <i className="fs-xs ti-angle-right mL-10"></i></a>
                    </span>
                  </li>
                </ul>
              </li>
              <li className="notifications dropdown">
                <span className="counter bgc-blue">3</span>
                <a href="" className="dropdown-toggle no-after" data-toggle="dropdown">
                  <i className="ti-email"></i>
                </a>

                <ul className="dropdown-menu">
                  <li className="pX-20 pY-15 bdB">
                    <i className="ti-email pR-10"></i>
                    <span className="fsz-sm fw-600 c-grey-900">Emails</span>
                  </li>
                  <li>
                    <ul className="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <div>
                              <div className="peers jc-sb fxw-nw mB-5">
                                <div className="peer">
                                  <p className="fw-500 mB-0">Premkumar</p>
                                </div>
                                <div className="peer">
                                  <small className="fsz-xs">5 mins ago</small>
                                </div>
                              </div>
                              <span className="c-grey-600 fsz-sm">
                                Want to create your own customized data generator for your app...
                              </span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <div>
                              <div className="peers jc-sb fxw-nw mB-5">
                                <div className="peer">
                                  <p className="fw-500 mB-0">Shyam </p>
                                </div>
                                <div className="peer">
                                  <small className="fsz-xs">15 mins ago</small>
                                </div>
                              </div>
                              <span className="c-grey-600 fsz-sm">
                                Want to create your own customized data generator for your app...
                              </span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="" className='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                          <div className="peer mR-15">
                            <img className="w-3r bdrs-50p" src="assets/static/images/unnamed.jpg" alt=""/>
                          </div>
                          <div className="peer peer-greed">
                            <div>
                              <div className="peers jc-sb fxw-nw mB-5">
                                <div className="peer">
                                  <p className="fw-500 mB-0">Bala</p>
                                </div>
                                <div className="peer">
                                  <small className="fsz-xs">25 mins ago</small>
                                </div>
                              </div>
                              <span className="c-grey-600 fsz-sm">
                                Want to create your own customized data generator for your app...
                              </span>
                            </div>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="pX-20 pY-15 ta-c bdT">
                    <span>
                      <a href="email.html" className="c-grey-600 cH-blue fsz-sm td-n">View All Email <i className="fs-xs ti-angle-right mL-10"></i></a>
                    </span>
                  </li>
                </ul>
              </li>
              <li className="dropdown">
                <a href="" className="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                  <div className="peer mR-10">
                    <img className="w-2r bdrs-50p" src="https://lh3.googleusercontent.com/a-/AAuE7mDeND1VYrt0Q_00R-KIKEf6yANle_tN_Tb7FW8M=s28-c-k-no" alt=""/>
                  </div>
                  <div className="peer">
                    <span className="fsz-sm c-grey-900">Prem Kumar</span>
                  </div>
                </a>
                <ul className="dropdown-menu fsz-sm">
                  <li>
                    <a href="" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                      <i className="ti-settings mR-10"></i>
                      <span>Setting</span>
                    </a>
                  </li>
                  <li>
                    <a href="" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                      <i className="ti-user mR-10"></i>
                      <span>Profile</span>
                    </a>
                  </li>
                  <li>
                    <a href="email.html" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                      <i className="ti-email mR-10"></i>
                      <span>Messages</span>
                    </a>
                  </li>
                  <li role="separator" className="divider"></li>
                  <li>
                    <a href="" className="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                      <i className="ti-power-off mR-10"></i>
                      <span>Logout</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul></div>
    </div>
           
            
    <div className={this.state.isSidebar ? "mobile-header blur-bg sidebar-show" : this.state.isProfile ? "mobile-header blur-bg profile-show" : "mobile-header "} >
      <ul>
        <li className="menu"><button onClick={this.addSidebarClass} className="active btn btn-sm btn-flat no-shadow waves-effect waves-explore btn-circle"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
	 viewBox="0 0 384.97 384.97"  ><g><g id="Menu_1_">
		<path d="M12.03,120.303h360.909c6.641,0,12.03-5.39,12.03-12.03c0-6.641-5.39-12.03-12.03-12.03H12.03
			c-6.641,0-12.03,5.39-12.03,12.03C0,114.913,5.39,120.303,12.03,120.303z"/>
		<path d="M372.939,180.455H12.03c-6.641,0-12.03,5.39-12.03,12.03s5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03
			S379.58,180.455,372.939,180.455z"/>
		<path d="M372.939,264.667H132.333c-6.641,0-12.03,5.39-12.03,12.03c0,6.641,5.39,12.03,12.03,12.03h240.606
			c6.641,0,12.03-5.39,12.03-12.03C384.97,270.056,379.58,264.667,372.939,264.667z"/></g><g></g><g></g><g></g><g></g><g></g><g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
</button> <a id='logo' className="sidebar-toggle" href="" >
                  LOGO
                </a></li>
       
        <li  className={this.state.isActive ? "profile expand-now" : "profile "}>
        <div className="search-bg-expand "></div><button onClick={this.addActiveClass} className="active btn btn-sm btn-flat no-shadow waves-effect waves-explore btn-circle"><svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 56.966 56.966">
<path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23
	s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92
	c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17
	s-17-7.626-17-17S14.61,6,23.984,6z"/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>

</button><button  onClick={this.addProfileClass} className="profile-btn btn btn-sm btn-flat no-shadow waves-effect waves-explore btn-circle" >
                  <div className="peer">
                    <img className="w-2r bdrs-50p" src="https://lh3.googleusercontent.com/a-/AAuE7mDeND1VYrt0Q_00R-KIKEf6yANle_tN_Tb7FW8M=s28-c-k-no" alt=""/>
                  </div>
                </button> <div className="search-content ">
                                      <form className="search-form">
                                      <button type="button" onClick={this.addActiveClass} className="back-button active btn btn-sm btn-flat no-shadow waves-effect waves-explore btn-circle">
                                      <svg version="1.1"  xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
	  viewBox="0 0 400.004 400.004"><g><path d="M382.688,182.686H59.116l77.209-77.214c6.764-6.76,6.764-17.726,0-24.485c-6.764-6.764-17.73-6.764-24.484,0L5.073,187.757
		c-6.764,6.76-6.764,17.727,0,24.485l106.768,106.775c3.381,3.383,7.812,5.072,12.242,5.072c4.43,0,8.861-1.689,12.242-5.072
		c6.764-6.76,6.764-17.726,0-24.484l-77.209-77.218h323.572c9.562,0,17.316-7.753,17.316-17.315
		C400.004,190.438,392.251,182.686,382.688,182.686z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg>
</button>
                        <input type="search" value="" placeholder="Search" className="search-input"/>
                        <button type="button" onClick={this.addActiveClass} className="clear-button active btn btn-sm btn-flat no-shadow waves-effect waves-explore btn-circle">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg>
</button>
                        
                        <button type="button" onClick={this.addActiveClass} className="search-button active btn btn-sm btn-flat no-shadow waves-effect waves-explore btn-circle"><svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 56.966 56.966">
<path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23
	s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92
	c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17
	s-17-7.626-17-17S14.61,6,23.984,6z"/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>

</button>
                        
                      </form>
                  </div></li>
        </ul>
        <div className="left-sidebar">
        <div class="logo"><img src="https://www.udemy.com/staticx/udemy/images/v6/logo-coral-light.svg"/> <button type="button" onClick={this.addSidebarClass} className="close-sidebar active btn btn-sm btn-flat no-shadow waves-effect waves-explore btn-circle">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg>
</button></div>
          <ul>
            <li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Jobs</span></li>
            <li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Events</span></li>
            <li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Calendar</span></li>
            <li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">My Timeline</span></li>
            <li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Resume</span></li>
          </ul>
        </div>
        <div className="profile-popup">
        <ul>
        <li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Home</span></li>
<li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Profile</span></li>
<li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Messages</span></li>
<li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Settings</span></li>
<li className="waves-effect"><span className="icon-li"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" ><g><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
</svg></span><span className="nav-text">Logout</span></li>
          </ul>
        </div>
    </div>
          </div>
        </div>
    )
}

}
export default header;
