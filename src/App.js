import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';  
import LoginLayoutRoute from "./LoginLayout";  
import LoginPage from './Login/LoginPage';
import SignUP from './Signup/signup';
import Forget from './Forget/forget';
import ForgetEmail from './Forget_Email/email';
import ForgetSms from './Forget_Email/sms';
import SmsVerify from './Forget_Email/sms_verify';
import Reset from './Forget_Email/reset';
import ResetSuccess from './Forget_Email/reset_success';
import EmailOTPVerify from './Forget_Email/email_otp_verify';
import PhoneOTPVerify from './Forget_Email/phone_otp_verify';
import Otp from './Login/Otp';
import NotFound from './404';

const NoMatch = ({ location }) => (
  <div>
    <h3>No match for <code>{location.pathname}</code></h3>
  </div>
)
function App() {
  return (
    <Router>  
        <Switch>  
          <Route exact path="/">  
            <Redirect to="/layout1" />  
          </Route>  
          <LoginLayoutRoute path="/login" component={LoginPage} />  
          <LoginLayoutRoute path="/forgetemail" component={ForgetEmail} /> 
          <LoginLayoutRoute path="/forgetsms" component={ForgetSms} />  
          <LoginLayoutRoute path="/reset" component={Reset} />  
          <LoginLayoutRoute path="/resetsuccess" component={ResetSuccess} />  
          <LoginLayoutRoute path="/smsverify" component={SmsVerify} />  
          <LoginLayoutRoute path="/emailverifyotp" component={EmailOTPVerify} />  
          <LoginLayoutRoute path="/phoneverifyotp" component={PhoneOTPVerify} />  
          <LoginLayoutRoute path="/forget" component={Forget} />  
          <LoginLayoutRoute path="/signup" component={SignUP} />  
          <LoginLayoutRoute path="/reset" component={Reset} />  
          <LoginLayoutRoute path="/forgetpassword" component={Forget} />  
          <LoginLayoutRoute path="/otp" component={Otp} /> 
          <Route component={NotFound} />
        </Switch>  
      </Router>  
  );
}

export default App;
