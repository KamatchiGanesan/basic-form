
    import React, { Component } from 'react'; 
    import "./login.css";  
    const LoginPage = ({  classes }) => {  
        return (  
            <div>
                 <div className="login-form user-section ">
                                    <div className="user-info">
                                  <div className="input-group">
                                  <div className=" input-effect">
                              <input className="effect-16"  name="username" type="text" placeholder=""/>
                              <label>Email-id / Phone No<span className="required">*</span><span className="error"></span></label>
                              <i className="clear"><svg height="511.992pt" viewBox="0 0 511.992 511.992" width="511.992pt" xmlns="http://www.w3.org/2000/svg"><path d="m415.402344 495.421875-159.40625-159.410156-159.40625 159.410156c-22.097656 22.09375-57.921875 22.09375-80.019532 0-22.09375-22.097656-22.09375-57.921875 0-80.019531l159.410157-159.40625-159.410157-159.40625c-22.09375-22.097656-22.09375-57.921875 0-80.019532 22.097657-22.09375 57.921876-22.09375 80.019532 0l159.40625 159.410157 159.40625-159.410157c22.097656-22.09375 57.921875-22.09375 80.019531 0 22.09375 22.097657 22.09375 57.921876 0 80.019532l-159.410156 159.40625 159.410156 159.40625c22.09375 22.097656 22.09375 57.921875 0 80.019531-22.097656 22.09375-57.921875 22.09375-80.019531 0zm0 0" fill="#000"/></svg></i>
                              <span className="error"><span className="info-icon"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
	 viewBox="0 0 512 512" >
<g>
	<g>
		<path d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M298.667,416
			c0,5.896-4.771,10.667-10.667,10.667h-64c-5.896,0-10.667-4.771-10.667-10.667V256h-10.667c-5.896,0-10.667-4.771-10.667-10.667
			v-42.667c0-5.896,4.771-10.667,10.667-10.667H288c5.896,0,10.667,4.771,10.667,10.667V416z M256,170.667
			c-23.531,0-42.667-19.135-42.667-42.667S232.469,85.333,256,85.333s42.667,19.135,42.667,42.667S279.531,170.667,256,170.667z"/>
	</g>
</g></svg></span>Please Fill the Values</span>
                              <span className="focus-border"></span>
                          </div></div>
                          <div className="input-group hide"><i className="icofont-ui-password"></i>
                          <div className=" input-effect">
                              <input className="effect-16"  type="password" name="password" placeholder=""/>
                              <label>Password <span className="required">*</span><span className="error"></span></label>
                              <span className="focus-border"></span>
                          </div>
                          </div>
                      </div>
                      <div className="row">
                      <div className="col-md-12 col-xs-12 text-center">
                          <button type="button" className="gradeinttext signin" >Login</button>
                      </div>
                      <div className="col-md-12 col-xs-12 text-center">
                      <a className="link forgetpassword-btn" >Forgot Password ?</a>
                      </div></div></div>
           </div>
        );  
      };  
      
      export default LoginPage  
