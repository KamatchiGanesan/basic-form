import React, { Component } from "react";
import "./login.css";
import axios from 'axios';
import { Link } from 'react-router-dom';
import Config from '../Config';

class LoginPage extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {},
      username_content:false,
      password_content:false,
      otp_content:false,
      loaderStatus: false,
      message: "",
      serverErrors: {},
      showPass: false,
      switch_uesr_pass: false,
      switch_otp_pass: false,
      username:"",
      username_error:"",
      password:"",
      password_error:""
    };
    this.handleLoginForm = this.handleLoginForm.bind(this);
    this.handleUserName = this.handleUserName.bind(this);
    this.showPass = this.showPass.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleSwitch = this.handleSwitch.bind(this);
  }
  handleSwitch(event){
    event.preventDefault();
    if(this.state.switch_otp_pass==false){
      this.setState({loaderStatus: true})
      this.state.fields["type"]=2;
      
      axios.post(Config[process.env.NODE_ENV + '_API_URL'] + '/react/send-login-otp',this.state.fields)
      .then(res => {
        if(res.status == 200){
          this.setState({loaderStatus: false})
          this.setState({message: res.data.message})
          this.setState(state => ({
            switch_otp_pass: !state.switch_otp_pass
          }));
        }
      }).catch((error)=>{
         this.setState({loaderStatus: true})
        this.setState({serverErrors: error.response.data.error});
      })
    }
    else{
      this.setState(state => ({
        switch_otp_pass: !state.switch_otp_pass
      }));
    }
  }
  handleOnChange(event){
    let fields = this.state.fields;
    fields[event.target.name] = event.target.value;
    this.setState({
      fields
    });
    if(event.target.value===""){
      this.setState({[event.target.name+"_content"]:false})
    }
    else{
      this.setState({[event.target.name+"_content"]:true})
    }
  }
  handleUserName(event){
    event.preventDefault();
    if (this.validateForm()) {
        axios.post(Config[process.env.NODE_ENV + '_API_URL'] + '/react/validate-login',this.state.fields)
      .then(res => {
        this.setState(state => ({
                showPass: !state.showPass,
                switch_uesr_pass: !state.switch_uesr_pass
            }));
      }).catch((error)=>{

            if(error.response.data.error){
                this.setState(state => ({
                    username_error: error.response.data.error.email_id
                }));    
                
                return false;
            }

                
      })
      
    }
  }
  handleLoginForm(event){
    event.preventDefault();
    if (this.validateLoginForm()) {
      this.setState({loaderStatus: true})
      
      if(this.state.switch_otp_pass==false){
        var data = {username:this.state.fields["username"], password:this.state.fields["password"]}
        axios.post(Config[process.env.NODE_ENV + '_API_URL'] + '/react/login', data)
        .then(res => {
          if(res.status == 200){
            this.setState({loaderStatus: false});
            window.location = Config[process.env.NODE_ENV + '_FRONT_URL'] + '/login?token=' + res.data.access_token;
          }
        }).catch((error)=>{
          this.setState({loaderStatus: false})
          this.setState({serverErrors: error.response.data})
        })
      }
      else{
        var data = {username:this.state.fields["username"], otp:this.state.fields["otp"]}
        axios.post(Config[process.env.NODE_ENV + '_API_URL'] + '/react/login/otp',data)
        .then(res => {
          if(res.status == 200){
            this.setState({loaderStatus: false})
            window.location = Config[process.env.NODE_ENV + '_API_URL'];
          }
        }).catch((error)=>{
          this.setState({loaderStatus: false});
          this.setState({serverErrors: error.response.data.errors})
        })
      }
    }
  }
  // handleClick() {
  //   this.setState(state => ({
  //     showPass: !state.showPass,
  //     switch_uesr_pass: !state.switch_uesr_pass
  //   }));
  // }
  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["username"]) {
      formIsValid = false;
      errors["username"] = "*Please enter your Name or Email";
    }
    this.setState({
      errors: errors
    });
    return formIsValid;
  }
  validateLoginForm(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;
    if(this.state.switch_otp_pass==true){
      if (!fields["otp"]) {
        formIsValid = false;
        errors["otp"] = "*Please enter your OTP.";
      }
  
      if (typeof fields["otp"] !== "undefined") {
        if (!fields["otp"].match(/^[0-9]{4}$/)) {
          formIsValid = false;
          errors["otp"] = "*Please enter valid OTP.";
        }
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
    }else{
      if (!fields["password"]) {
        formIsValid = false;
        errors["password"] = "*Please enter your password.";
      }
      if (typeof fields["password"] !== "undefined") {
        if (!fields["password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
          formIsValid = false;
          errors["password"] = "*Please enter secure and strong password.";
        }
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
    }
  }

  showPass() {
    this.setState(state => ({
      showPass: !state.showPass
    }));
  }

  render() {
    return (
      <div>
        <h4 className="response-message">{this.state.message}</h4>
        <div className="login">
          <div className="login-page dark login-page-background">
            <div className="scroll-login">
              <div className="login-card">
                <div className={this.state.loaderStatus ? "loader display-block":"loader display-none"}>
                  <div class="loaderBar"></div>
                </div>
                <div className="login-card_section_two">
                  <div className="card-body-section-center">
                    <div className="center-flex">
                      <div className="login-card-header no-pad">
                        <div>
                          <h2>Login</h2>
                          <small>Using your கண்டறிவது  account </small>
                        </div>
                      </div>
                      <div className="login-card-body no-pad">
                        <form autoComplete="off" onSubmit={this.handleLoginForm}>
                          <div
                            className={
                              this.state.switch_uesr_pass
                                ? "login-form user-section goleft"
                                : "login-form user-section "
                            }
                          >
                            <div className="user-info">
                              <div className="input-group">
                                <div className=" input-effect">
                                  <input
                                    className={this.state.username_content ? "effect-16 has-content":"effect-16"}
                                    name="username"
                                    type="text"
                                    placeholder=""
                                    value={this.state.fields.username} 
                                    onChange={this.handleOnChange}
                                  />
                                  <label>
                                    Email-id / Phone No
                                    <span className="required">*</span>
                                    <span className="error"></span>
                                  </label>
                                  <div className="abs-btn-icons">
                                    {" "}
                                    <i className="clear">
                                      <svg
                                        height="511.992pt"
                                        viewBox="0 0 511.992 511.992"
                                        width="511.992pt"
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <path
                                          d="m415.402344 495.421875-159.40625-159.410156-159.40625 159.410156c-22.097656 22.09375-57.921875 22.09375-80.019532 0-22.09375-22.097656-22.09375-57.921875 0-80.019531l159.410157-159.40625-159.410157-159.40625c-22.09375-22.097656-22.09375-57.921875 0-80.019532 22.097657-22.09375 57.921876-22.09375 80.019532 0l159.40625 159.410157 159.40625-159.410157c22.097656-22.09375 57.921875-22.09375 80.019531 0 22.09375 22.097657 22.09375 57.921876 0 80.019532l-159.410156 159.40625 159.410156 159.40625c22.09375 22.097656 22.09375 57.921875 0 80.019531-22.097656 22.09375-57.921875 22.09375-80.019531 0zm0 0"
                                          fill="#000"
                                        />
                                      </svg>
                                    </i>
                                  </div>

                                  <span className="focus-border"></span>
                                </div>
                                <span className="error">
                                  <span className="info-icon">
                                    <svg
                                      version="1.1"
                                      id="Capa_1"
                                      xmlns="http://www.w3.org/2000/svg"
                                      x="0px"
                                      y="0px"
                                      viewBox="0 0 512 512"
                                    >
                                      <g>
                                        <g>
                                          <path
                                            d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M298.667,416
    c0,5.896-4.771,10.667-10.667,10.667h-64c-5.896,0-10.667-4.771-10.667-10.667V256h-10.667c-5.896,0-10.667-4.771-10.667-10.667
    v-42.667c0-5.896,4.771-10.667,10.667-10.667H288c5.896,0,10.667,4.771,10.667,10.667V416z M256,170.667
    c-23.531,0-42.667-19.135-42.667-42.667S232.469,85.333,256,85.333s42.667,19.135,42.667,42.667S279.531,170.667,256,170.667z"
                                          />
                                        </g>
                                      </g>
                                    </svg>
                                  </span>
                                  <div className="errorMsg">{this.state.username_error}</div>
                                </span>
                              </div>
                            </div>
                            

                            <div className="row">
                              <div className="col-md-12 col-xs-12 text-left">
                                <Link to="/forget" className="link forgetpassword-btn">
                                  Forgot Password ?
                                </Link>
                              </div>
                              <div className="col-md-12 col-xs-12 text-right">
                                <button
                                  type="button"
                                  className="gradeinttext signin"
                                  onClick={this.handleUserName}
                                >
                                  Next
                                </button>
                              </div>

                              <div className="col-md-12 col-xs-12 text-center mt-10">
                                <span>Do not have an account? </span>
                                <a className="link signup-btn"><Link to='/signup'>Sign Up</Link></a>
                              </div>
                            </div>
                          </div>
                          <div
                            className={
                              this.state.switch_uesr_pass
                                ? "login-form pass-section goleft"
                                : "login-form pass-section goright"
                            }
                          >
                            <h4 onClick={this.handleUserName} className="back-to-username">Username: {this.state.fields.username}</h4>
                            <div className="user-info">
                              <div className="switch-section">
                              <div  className={this.state.switch_otp_pass? "password-section switch goleft": "password-section switch "}>
                              <div className="input-group">
                                <div className=" input-effect">
                                  <input
                                    className={this.state.username_content ? "effect-16 has-content":"effect-16"}
                                    name="password"
                                    type={
                                      this.state.showPass ? "password" : "text"
                                    }
                                    placeholder=""
                                    value={this.state.fields.password} 
                                    onChange={this.handleOnChange}
                                  />
                                  <label>
                                    Password<span className="required">*</span>
                                    <span className="error"></span>
                                  </label>
                                  <div className="abs-btn-icons">
                                    {this.state.showPass ? (
                                      <i
                                        className="eye"
                                        onClick={this.showPass}
                                      >
                                        <svg
                                          version="1.1"
                                          id="Capa_1"
                                          xmlns="http://www.w3.org/2000/svg"
                                          x="0px"
                                          y="0px"
                                          viewBox="0 0 511.992 511.992"
                                        >
                                          <g>
                                            <g>
                                              <path
                                                d="M510.096,249.937c-4.032-5.867-100.928-143.275-254.101-143.275C124.56,106.662,7.44,243.281,2.512,249.105
			c-3.349,3.968-3.349,9.792,0,13.781C7.44,268.71,124.56,405.329,255.995,405.329S504.549,268.71,509.477,262.886
			C512.571,259.217,512.848,253.905,510.096,249.937z M255.995,383.996c-105.365,0-205.547-100.48-230.997-128
			c25.408-27.541,125.483-128,230.997-128c123.285,0,210.304,100.331,231.552,127.424
			C463.013,282.065,362.256,383.996,255.995,383.996z"
                                              />
                                            </g>
                                          </g>
                                          <g>
                                            <g>
                                              <path
                                                d="M255.995,170.662c-47.061,0-85.333,38.272-85.333,85.333s38.272,85.333,85.333,85.333s85.333-38.272,85.333-85.333
			S303.056,170.662,255.995,170.662z M255.995,319.996c-35.285,0-64-28.715-64-64s28.715-64,64-64s64,28.715,64,64
			S291.28,319.996,255.995,319.996z"
                                              />
                                            </g>
                                          </g>
                                        </svg>
                                      </i>
                                    ) : (
                                      <i
                                        className="eye"
                                        onClick={this.showPass}
                                      >
                                        <svg
                                          version="1.1"
                                          id="Capa_1"
                                          xmlns="http://www.w3.org/2000/svg"
                                          x="0px"
                                          y="0px"
                                          viewBox="0 0 512.001 512.001"
                                        >
                                          <g>
                                            <g>
                                              <path
                                                d="M316.332,195.662c-4.16-4.16-10.923-4.16-15.083,0c-4.16,4.16-4.16,10.944,0,15.083
			c12.075,12.075,18.752,28.139,18.752,45.248c0,35.285-28.715,64-64,64c-17.109,0-33.173-6.656-45.248-18.752
			c-4.16-4.16-10.923-4.16-15.083,0c-4.16,4.139-4.16,10.923,0,15.083c16.085,16.128,37.525,25.003,60.331,25.003
			c47.061,0,85.333-38.272,85.333-85.333C341.334,233.187,332.46,211.747,316.332,195.662z"
                                              />
                                            </g>
                                          </g>
                                          <g>
                                            <g>
                                              <path
                                                d="M270.87,172.131c-4.843-0.853-9.792-1.472-14.869-1.472c-47.061,0-85.333,38.272-85.333,85.333
			c0,5.077,0.619,10.027,1.493,14.869c0.917,5.163,5.419,8.811,10.475,8.811c0.619,0,1.237-0.043,1.877-0.171
			c5.781-1.024,9.664-6.571,8.64-12.352c-0.661-3.627-1.152-7.317-1.152-11.157c0-35.285,28.715-64,64-64
			c3.84,0,7.531,0.491,11.157,1.131c5.675,1.152,11.328-2.859,12.352-8.64C280.534,178.702,276.652,173.155,270.87,172.131z"
                                              />
                                            </g>
                                          </g>
                                          <g>
                                            <g>
                                              <path
                                                d="M509.462,249.102c-2.411-2.859-60.117-70.208-139.712-111.445c-5.163-2.709-11.669-0.661-14.379,4.587
			c-2.709,5.227-0.661,11.669,4.587,14.379c61.312,31.744,110.293,81.28,127.04,99.371c-25.429,27.541-125.504,128-230.997,128
			c-35.797,0-71.872-8.64-107.264-25.707c-5.248-2.581-11.669-0.341-14.229,4.971c-2.581,5.291-0.341,11.669,4.971,14.229
			c38.293,18.496,77.504,27.84,116.523,27.84c131.435,0,248.555-136.619,253.483-142.443
			C512.854,258.915,512.833,253.091,509.462,249.102z"
                                              />
                                            </g>
                                          </g>
                                          <g>
                                            <g>
                                              <path
                                                d="M325.996,118.947c-24.277-8.171-47.829-12.288-69.995-12.288c-131.435,0-248.555,136.619-253.483,142.443
			c-3.115,3.669-3.371,9.003-0.597,12.992c1.472,2.112,36.736,52.181,97.856,92.779c1.813,1.216,3.84,1.792,5.888,1.792
			c3.435,0,6.827-1.664,8.875-4.8c3.264-4.885,1.92-11.52-2.987-14.763c-44.885-29.845-75.605-65.877-87.104-80.533
			c24.555-26.667,125.291-128.576,231.552-128.576c19.861,0,41.131,3.755,63.189,11.157c5.589,2.005,11.648-1.088,13.504-6.699
			C334.572,126.862,331.585,120.825,325.996,118.947z"
                                              />
                                            </g>
                                          </g>
                                          <g>
                                            <g>
                                              <path
                                                d="M444.865,67.128c-4.16-4.16-10.923-4.16-15.083,0L67.116,429.795c-4.16,4.16-4.16,10.923,0,15.083
			c2.091,2.069,4.821,3.115,7.552,3.115c2.731,0,5.461-1.045,7.531-3.115L444.865,82.211
			C449.025,78.051,449.025,71.288,444.865,67.128z"
                                              />
                                            </g>
                                          </g>
                                        </svg>
                                      </i>
                                    )}
                                    <i className="clear">
                                      <svg
                                        height="511.992pt"
                                        viewBox="0 0 511.992 511.992"
                                        width="511.992pt"
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <path
                                          d="m415.402344 495.421875-159.40625-159.410156-159.40625 159.410156c-22.097656 22.09375-57.921875 22.09375-80.019532 0-22.09375-22.097656-22.09375-57.921875 0-80.019531l159.410157-159.40625-159.410157-159.40625c-22.09375-22.097656-22.09375-57.921875 0-80.019532 22.097657-22.09375 57.921876-22.09375 80.019532 0l159.40625 159.410157 159.40625-159.410157c22.097656-22.09375 57.921875-22.09375 80.019531 0 22.09375 22.097657 22.09375 57.921876 0 80.019532l-159.410156 159.40625 159.410156 159.40625c22.09375 22.097656 22.09375 57.921875 0 80.019531-22.097656 22.09375-57.921875 22.09375-80.019531 0zm0 0"
                                          fill="#000"
                                        />
                                      </svg>
                                    </i>
                                  </div>

                                  <span className="focus-border"></span>
                                </div>
                                <span className="error">
                                  <span className="info-icon">
                                    <svg
                                      version="1.1"
                                      id="Capa_1"
                                      xmlns="http://www.w3.org/2000/svg"
                                      x="0px"
                                      y="0px"
                                      viewBox="0 0 512 512"
                                    >
                                      <g>
                                        <g>
                                          <path
                                            d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M298.667,416
    c0,5.896-4.771,10.667-10.667,10.667h-64c-5.896,0-10.667-4.771-10.667-10.667V256h-10.667c-5.896,0-10.667-4.771-10.667-10.667
    v-42.667c0-5.896,4.771-10.667,10.667-10.667H288c5.896,0,10.667,4.771,10.667,10.667V416z M256,170.667
    c-23.531,0-42.667-19.135-42.667-42.667S232.469,85.333,256,85.333s42.667,19.135,42.667,42.667S279.531,170.667,256,170.667z"
                                          />
                                        </g>
                                      </g>
                                    </svg>
                                  </span>
                                  {/* {this.state.password_error} */}
                                </span>
                              </div>
                              <div className="errorMsg">{this.state.errors.password}</div>
                              </div>
                              <div  className={this.state.switch_otp_pass? "otp-section switch goleft": "otp-section switch "}> 
                              <div className="input-group">
                                <div className=" input-effect">
                                  <input
                                    className={this.state.otp_content ? "effect-16 has-content":"effect-16"}
                                    name="otp"
                                    type="text"
                                    placeholder=""
                                    value={this.state.fields.otp} 
                                    onChange={this.handleOnChange}
                                  />
                                  <label>
                                    Otp<span className="required">*</span>
                                    <span className="error"></span>
                                  </label>
                                  <div className="abs-btn-icons">
                                   
                                    
                                    <i className="clear">
                                      <svg
                                        height="511.992pt"
                                        viewBox="0 0 511.992 511.992"
                                        width="511.992pt"
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <path
                                          d="m415.402344 495.421875-159.40625-159.410156-159.40625 159.410156c-22.097656 22.09375-57.921875 22.09375-80.019532 0-22.09375-22.097656-22.09375-57.921875 0-80.019531l159.410157-159.40625-159.410157-159.40625c-22.09375-22.097656-22.09375-57.921875 0-80.019532 22.097657-22.09375 57.921876-22.09375 80.019532 0l159.40625 159.410157 159.40625-159.410157c22.097656-22.09375 57.921875-22.09375 80.019531 0 22.09375 22.097657 22.09375 57.921876 0 80.019532l-159.410156 159.40625 159.410156 159.40625c22.09375 22.097656 22.09375 57.921875 0 80.019531-22.097656 22.09375-57.921875 22.09375-80.019531 0zm0 0"
                                          fill="#000"
                                        />
                                      </svg>
                                    </i>
                                  </div>

                                  <span className="focus-border"></span>
                                </div>
                                <span className="error">
                                  <span className="info-icon">
                                    <svg
                                      version="1.1"
                                      id="Capa_1"
                                      xmlns="http://www.w3.org/2000/svg"
                                      x="0px"
                                      y="0px"
                                      viewBox="0 0 512 512"
                                    >
                                      <g>
                                        <g>
                                          <path
                                            d="M256,0C114.844,0,0,114.844,0,256s114.844,256,256,256s256-114.844,256-256S397.156,0,256,0z M298.667,416
    c0,5.896-4.771,10.667-10.667,10.667h-64c-5.896,0-10.667-4.771-10.667-10.667V256h-10.667c-5.896,0-10.667-4.771-10.667-10.667
    v-42.667c0-5.896,4.771-10.667,10.667-10.667H288c5.896,0,10.667,4.771,10.667,10.667V416z M256,170.667
    c-23.531,0-42.667-19.135-42.667-42.667S232.469,85.333,256,85.333s42.667,19.135,42.667,42.667S279.531,170.667,256,170.667z"
                                          />
                                        </g>
                                      </g>
                                    </svg>
                                  </span>
                                  {this.state.password_error}
                                </span>
                              </div></div>

                              <div className="errorMsg">{this.state.errors.otp}</div>
                              <div className="errorMsg">{this.state.serverErrors?.email_id}</div>
                              <div className="errorMsg">{this.state.serverErrors?.error}</div>
                              <div className="errorMsg">{this.state.serverErrors?.otp}</div>
                              <div class="cearfix">&nbsp;</div>
                              </div>
                             
                            </div>
                            <div className="row no-mar">
                            <div className="col-md-12 col-xs-12 text-left">
                              <p className="info"><span>Want to Login using OTP?</span></p>
                                <button type="button" className="link forgetpassword-btn" onClick={this.handleSwitch}>
                                Send OTP
                                </button>
                              </div>
                              <div className="col-md-12 col-xs-12 text-left">
                                
                                <a className="link forgetpassword-btn">
                                  <Link to="">Forgot Password ?</Link>
                                </a>
                              </div>
                             
                              <div className="col-md-12 col-xs-12 text-right">
                                <button
                                  type="submit"
                                  className="gradeinttext signin"
                                  onClick={this.handleClick}
                                >
                                  Login
                                </button>
                              </div>
                              <div className="col-md-12 col-xs-12 text-center mt-10">
                                <span>Do not have an account? </span>
                                <a className="link signup-btn"><Link to="/signup">Sign Up</Link></a>
                              </div>
                            </div>
                          </div>
                        </form>
                        <div className="row">
                          <div className="social-login">
                            <ul>
                              <li>
                                <a class="btn btn-block btn-social btn-facebook">
                                  <span class="fa fa-facebook">
                                    <svg
                                      version="1.1"
                                      id="Capa_1"
                                      xmlns="http://www.w3.org/2000/svg"
                                      x="0px"
                                      y="0px"
                                      viewBox="0 0 112.196 112.196"
                                    >
                                      <g>
                                        <circle
                                          style={{ fill: "#3B5998" }}
                                          cx="56.098"
                                          cy="56.098"
                                          r="56.098"
                                        />
                                        <path
                                          style={{ fill: "#FFFFFF" }}
                                          d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
 c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"
                                        />
                                      </g>
                                    </svg>
                                  </span>
                                </a>
                              </li>
                              <li>
                                <a class="btn btn-block btn-social btn-twitter">
                                  <span class="fa fa-facebook"></span>
                                  <svg
                                    version="1.1"
                                    id="Capa_1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    x="0px"
                                    y="0px"
                                    viewBox="0 0 112.197 112.197"
                                  >
                                    <g>
                                      <circle
                                        style={{ fill: "#55ACEE" }}
                                        cx="56.099"
                                        cy="56.098"
                                        r="56.098"
                                      />
                                      <g>
                                        <path
                                          style={{ fill: "#F1F2F2" }}
                                          d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
   c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
   c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
   c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
   c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
   c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
   c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
   C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"
                                        />
                                      </g>
                                    </g>
                                  </svg>{" "}
                                </a>
                              </li>
                              <li>
                                <a class="btn btn-block btn-social btn-twitter">
                                  <span class="fa fa-facebook"></span>
                                  <svg
                                    version="1.1"
                                    id="Capa_1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    x="0px"
                                    y="0px"
                                    viewBox="0 0 112.196 112.196"
                                  >
                                    <g>
                                      <circle
                                        style={{ fill: "#007AB9" }}
                                        cx="56.098"
                                        cy="56.097"
                                        r="56.098"
                                      />
                                      <g>
                                        <path
                                          style={{ fill: "#F1F2F2" }}
                                          d="M89.616,60.611v23.128H76.207V62.161c0-5.418-1.936-9.118-6.791-9.118
   c-3.705,0-5.906,2.491-6.878,4.903c-0.353,0.862-0.444,2.059-0.444,3.268v22.524H48.684c0,0,0.18-36.546,0-40.329h13.411v5.715
   c-0.027,0.045-0.065,0.089-0.089,0.132h0.089v-0.132c1.782-2.742,4.96-6.662,12.085-6.662
   C83.002,42.462,89.616,48.226,89.616,60.611L89.616,60.611z M34.656,23.969c-4.587,0-7.588,3.011-7.588,6.967
   c0,3.872,2.914,6.97,7.412,6.97h0.087c4.677,0,7.585-3.098,7.585-6.97C42.063,26.98,39.244,23.969,34.656,23.969L34.656,23.969z
    M27.865,83.739H41.27V43.409H27.865V83.739z"
                                        />
                                      </g>
                                    </g>
                                  </svg>
                                </a>
                              </li>
                              <li>
                                <a class="btn btn-block btn-social btn-whatsapp">
                                  <span class="fa fa-facebook"></span>
                                  <svg
                                    version="1.1"
                                    id="Capa_1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    x="0px"
                                    y="0px"
                                    viewBox="0 0 512 512"
                                  >
                                    <path
                                      style={{ fill: "#4CAF50" }}
                                      d="M256.064,0h-0.128l0,0C114.784,0,0,114.816,0,256c0,56,18.048,107.904,48.736,150.048l-31.904,95.104
l98.4-31.456C155.712,496.512,204,512,256.064,512C397.216,512,512,397.152,512,256S397.216,0,256.064,0z"
                                    />
                                    <path
                                      style={{ fill: "#FAFAFA" }}
                                      d="M405.024,361.504c-6.176,17.44-30.688,31.904-50.24,36.128c-13.376,2.848-30.848,5.12-89.664-19.264
C189.888,347.2,141.44,270.752,137.664,265.792c-3.616-4.96-30.4-40.48-30.4-77.216s18.656-54.624,26.176-62.304
c6.176-6.304,16.384-9.184,26.176-9.184c3.168,0,6.016,0.16,8.576,0.288c7.52,0.32,11.296,0.768,16.256,12.64
c6.176,14.88,21.216,51.616,23.008,55.392c1.824,3.776,3.648,8.896,1.088,13.856c-2.4,5.12-4.512,7.392-8.288,11.744
c-3.776,4.352-7.36,7.68-11.136,12.352c-3.456,4.064-7.36,8.416-3.008,15.936c4.352,7.36,19.392,31.904,41.536,51.616
c28.576,25.44,51.744,33.568,60.032,37.024c6.176,2.56,13.536,1.952,18.048-2.848c5.728-6.176,12.8-16.416,20-26.496
c5.12-7.232,11.584-8.128,18.368-5.568c6.912,2.4,43.488,20.48,51.008,24.224c7.52,3.776,12.48,5.568,14.304,8.736
C411.2,329.152,411.2,344.032,405.024,361.504z"
                                    />
                                  </svg>
                                </a>
                              </li>
                              <li>
                                <a class="btn btn-block btn-social btn-whatsapp">
                                  <span class="fa fa-facebook"></span>
                                  <svg
                                    version="1.1"
                                    id="Capa_1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    x="0px"
                                    y="0px"
                                    viewBox="0 0 112.196 112.196"
                                  >
                                    <g>
                                      <g>
                                        <circle
                                          id="XMLID_30_"
                                          style={{ fill: "#DC4E41" }}
                                          cx="56.098"
                                          cy="56.097"
                                          r="56.098"
                                        />
                                      </g>
                                      <g>
                                        <path
                                          style={{ fill: "#DC4E41" }}
                                          d="M19.531,58.608c-0.199,9.652,6.449,18.863,15.594,21.867c8.614,2.894,19.205,0.729,24.937-6.648
   c4.185-5.169,5.136-12.06,4.683-18.498c-7.377-0.066-14.754-0.044-22.12-0.033c-0.012,2.628,0,5.246,0.011,7.874
   c4.417,0.122,8.835,0.066,13.252,0.155c-1.115,3.821-3.655,7.377-7.51,8.757c-7.443,3.28-16.94-1.005-19.282-8.813
   c-2.827-7.477,1.801-16.5,9.442-18.675c4.738-1.667,9.619,0.21,13.673,2.673c2.054-1.922,3.976-3.976,5.864-6.052
   c-4.606-3.854-10.525-6.217-16.61-5.698C29.526,35.659,19.078,46.681,19.531,58.608z"
                                        />
                                        <path
                                          style={{ fill: "#DC4E41" }}
                                          d="M79.102,48.668c-0.022,2.198-0.045,4.407-0.056,6.604c-2.209,0.022-4.406,0.033-6.604,0.044
   c0,2.198,0,4.384,0,6.582c2.198,0.011,4.407,0.022,6.604,0.045c0.022,2.198,0.022,4.395,0.044,6.604c2.187,0,4.385-0.011,6.582,0
   c0.012-2.209,0.022-4.406,0.045-6.615c2.197-0.011,4.406-0.022,6.604-0.033c0-2.198,0-4.384,0-6.582
   c-2.197-0.011-4.406-0.022-6.604-0.044c-0.012-2.198-0.033-4.407-0.045-6.604C83.475,48.668,81.288,48.668,79.102,48.668z"
                                        />
                                        <g>
                                          <path
                                            style={{ fill: "#FFFFFF" }}
                                            d="M19.531,58.608c-0.453-11.927,9.994-22.949,21.933-23.092c6.085-0.519,12.005,1.844,16.61,5.698
     c-1.889,2.077-3.811,4.13-5.864,6.052c-4.054-2.463-8.935-4.34-13.673-2.673c-7.642,2.176-12.27,11.199-9.442,18.675
     c2.342,7.808,11.839,12.093,19.282,8.813c3.854-1.38,6.395-4.936,7.51-8.757c-4.417-0.088-8.835-0.033-13.252-0.155
     c-0.011-2.628-0.022-5.246-0.011-7.874c7.366-0.011,14.743-0.033,22.12,0.033c0.453,6.439-0.497,13.33-4.683,18.498
     c-5.732,7.377-16.322,9.542-24.937,6.648C25.981,77.471,19.332,68.26,19.531,58.608z"
                                          />
                                          <path
                                            style={{ fill: "#FFFFFF" }}
                                            d="M79.102,48.668c2.187,0,4.373,0,6.57,0c0.012,2.198,0.033,4.407,0.045,6.604
     c2.197,0.022,4.406,0.033,6.604,0.044c0,2.198,0,4.384,0,6.582c-2.197,0.011-4.406,0.022-6.604,0.033
     c-0.022,2.209-0.033,4.406-0.045,6.615c-2.197-0.011-4.396,0-6.582,0c-0.021-2.209-0.021-4.406-0.044-6.604
     c-2.197-0.023-4.406-0.033-6.604-0.045c0-2.198,0-4.384,0-6.582c2.198-0.011,4.396-0.022,6.604-0.044
     C79.057,53.075,79.079,50.866,79.102,48.668z"
                                          />
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="login-card-footer"></div>
                    </div>
                  </div>
                </div>
                <div className="login-card_section_one text-center">
                  <div className="card-body-section-center">
                    <center>
                      <img src="" className="card-logo" />
                    </center>
                    <h1>Title</h1>
                    <p>
                      {" "}
                      Welcome to <b> CloudEnsure</b> Monitor your AWS Resources,
                      Best Practices Compliances & Optimize your spend.
                    </p>
                  </div>
                </div>

                <div className="footer-mobile">
                  Licensed By &#169; aalavai.com . All rights reserved.
                </div>
              </div>
            </div>{" "}
            <div className="footer">
              Licensed By &#169; aalavai.com . All rights reserved.
            </div>{" "}
          </div>
        </div>{" "}
      </div>
    );
  }
}

export default LoginPage;
