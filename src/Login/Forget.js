
    import React, { Component } from 'react'; 
    import "./login.css";  
    const LoginPage = ({  classes }) => {  
        return (  
            <div>
            <div class="login">
    <div class="login-page dark login-page-background" ><div class="scroll-login">
        <div class="waveWrapper waveAnimation">
  <div class="waveWrapperInner bgTop">
    <div class="wave waveTop" ></div>
  </div>
  <div class="waveWrapperInner bgMiddle">
    <div class="wave waveMiddle" ></div>
  </div>
  <div class="waveWrapperInner bgBottom">
    <div class="wave waveBottom" ></div>
  </div>
</div>
          <div class="login-card">
          <div class="login-card_section_one">

           <div class="card-body-section-center">
              <center><img  class="card-logo"/></center>
              <h1>Explore</h1>
              <p> Welcome to <b> CloudEnsure</b> Monitor your AWS Resources, Best Practices Compliances & Optimize your spend.</p>
              </div>

          </div>
          <div class="login-card_section_two">

              <div class="card-body-section-center">
                <div class="center-flex">
           <div class="login-card-header no-pad"><center><small>Please enter the following details to </small><h2>Recover Password</h2></center></div>
           <div class="login-card-body no-pad">
              <form>
                  <div class ="login-form">
                  <div class="input-group"><i class="icofont-user-alt-1"></i>
                  <div class=" input-effect">
            <input class="effect-16"  name="username" type="text" placeholder=""/>
            <label>Email-id / Phone No<span class="required">*</span><span class="error"></span></label>
            <span class="focus-border"></span>
        </div></div>
        
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-xs-6 text-center">
                        <button type="button" class="gradeinttext signin" >Email</button>
                    </div>
                    <div class="col-md-6 col-xs-6 text-center">
                        <button type="button" class="gradeinttext signin" >SMS</button>
                    </div>
                  
                    <div class="col-md-12 col-xs-12 text-center mt-10">
                      <span>Back To  </span>
                      <a class="link signup-btn" href="#"> LOGIN</a>
                     </div>
                  </div>


              </form>
              </div>
              <div class="login-card-footer"></div>
              </div>
              </div>
          </div>
          <div className="footer-mobile">Licensed By &#169; aalavai.com . All rights reserved.</div> 
          </div>

          </div> <div class="footer">Licensed By &#169; aalavai.com . All rights reserved.</div> </div>
        </div>
 </div> 
        );  
      };  
      
      export default LoginPage  
