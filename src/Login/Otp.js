
    import React, { Component } from 'react'; 
    import "./login.css";
    import "./otp_main.js";  
    const LoginPage = ({  classes }) => {  
        return (  
            <div>
           <div className="login">
    <div className="login-page dark login-page-background" ><div className="scroll-login">
        <div className="waveWrapper waveAnimation">
  <div className="waveWrapperInner bgTop">
    <div className="wave waveTop" ></div>
  </div>
  <div className="waveWrapperInner bgMiddle">
    <div className="wave waveMiddle" ></div>
  </div>
  <div className="waveWrapperInner bgBottom">
    <div className="wave waveBottom" ></div>
  </div>
</div>
          <div className="login-card">
          <div className="login-card_section_one">

           <div className="card-body-section-center">
              <center><img src="" className="card-logo"/></center>
              <h1>Explore</h1>
              <p> Welcome to <b> CloudEnsure</b> Monitor your AWS Resources, Best Practices Compliances & Optimize your spend.</p>
              </div>

          </div>
          <div className="login-card_section_two">

              <div className="card-body-section-center">
                <div className="center-flex">
           <div className="login-card-header no-pad"><center><small>Please enter the following <b>SMS</b> Sent to <span className="number-sms">XXXXX6433</span> </small><h2>Recover Password</h2></center></div>
           <div className="login-card-body no-pad">
              <form>
                  <div class ="login-form otp">
                  <div className="input-group">
                  <div className=" input-effect">
            <input className="effect-16" id="OTP1" placeholder="-" maxlength="1" name="username" type="text" placeholder="" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)"/>
          
            <span className="focus-border"></span>
        </div></div>
        <div className="input-group">
          <div className=" input-effect">
    <input className="effect-16" id="OTP2" placeholder="-" maxlength="1" name="username" type="text" placeholder="" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(1)"/>
   
    <span className="focus-border"></span>
</div></div>
<div className="input-group">
  <div className=" input-effect">
<input className="effect-16" id="OTP3" placeholder="-" maxlength="1" name="username" type="text" placeholder="" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(1)"/>

<span className="focus-border"></span>
</div></div>
<div className="input-group">
  <div className=" input-effect">
<input className="effect-16" id="OTP4" placeholder="-" maxlength="1"  name="username" type="text" placeholder="" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(1)"/>

<span className="focus-border"></span>
</div></div>
        
                  </div>
                  <div className="row">
                    <div className="col-md-12 col-xs-12 text-center">
                        <button type="button" className="gradeinttext signin" >Verify</button>
                    </div>
                    <div className="col-md-12 col-xs-12 text-center">
                        <a href="#" className="link forgetpassword-btn" >Resend <b>OTP</b> </a>
                       </div>
                  
                    <div className="col-md-12 col-xs-12 text-center mt-10">
                      <span>Back To  </span>
                      <a className="link signup-btn" href="#"> LOGIN</a>
                     </div>
                  </div>


              </form>
              </div>
              <div className="login-card-footer"></div>
              </div>
              </div>
          </div>
          <div className="footer-mobile">Licensed By &#169; aalavai.com . All rights reserved.</div> 
          </div>

          </div> <div className="footer">Licensed By &#169; aalavai.com . All rights reserved.</div> </div>
        </div> </div> 
        );  
      };  
      
      export default LoginPage  
