
    import React, { Component } from 'react'; 
    import "./login.css";  
    const LoginPage = ({  classes }) => {  
      
        return (  
            <div>
           <div className="login">
    <div className="login-page dark login-page-background" ><div className="scroll-login">
        <div className="waveWrapper waveAnimation">
  <div className="waveWrapperInner bgTop">
    <div className="wave waveTop" ></div>
  </div>
  <div className="waveWrapperInner bgMiddle">
    <div className="wave waveMiddle" ></div>
  </div>
  <div className="waveWrapperInner bgBottom">
    <div className="wave waveBottom" ></div>
  </div>
</div>
          <div className="login-card">
          <div className="login-card_section_one">

           <div className="card-body-section-center">
              <center><img  className="card-logo"/></center>
              <h1>Explore</h1>
              <p> Welcome to <b> CloudEnsure</b> Monitor your AWS Resources, Best Practices Compliances & Optimize your spend.</p>
              </div>

          </div>
          <div className="login-card_section_two">

              <div className="card-body-section-center">
                <div className="center-flex">
           <div className="login-card-header no-pad"><center><small>Please enter the following details to </small><h2>Set Password</h2></center></div>
           <div className="login-card-body no-pad">
              <form>
                  <div class ="login-form">
                    <div className="input-group"><i className="icofont-ui-password"></i>
                        <div className=" input-effect">
                            <input className="effect-16"  type="password" name="password" placeholder=""/>
                            <label>New Password <span className="required">*</span><span className="error"></span></label>
                            <span className="focus-border"></span>
                        </div>
                        </div>
                        <div className="input-group"><i className="icofont-ui-password"></i>
                            <div className=" input-effect">
                                <input className="effect-16"  type="password" name="password" placeholder=""/>
                                <label>Confirm Password <span className="required">*</span><span className="error"></span></label>
                                <span className="focus-border"></span>
                            </div>
                            </div>
        
        
                  </div>
                  <div className="row">
                    <div className="col-md-12 col-xs-12 text-center">
                        <button type="button" className="gradeinttext signin" >Reset</button>
                    </div>
                  
                    <div className="col-md-12 col-xs-12 text-center mt-10">
                      <span>Back To  </span>
                      <a className="link signup-btn" href="#"> LOGIN</a>
                     </div>
                  </div>


              </form>
              </div>
              <div className="login-card-footer"></div>
              </div>
              </div>
          </div>
          <div className="footer-mobile">Licensed By &#169; aalavai.com . All rights reserved.</div> 
          </div>

          </div> <div className="footer">Licensed By &#169; aalavai.com . All rights reserved.</div> </div>
        </div>
           
            </div> 
        );  
      };  
      
      export default LoginPage  
