import React, { Component } from "react";
import "./forget.css";
import { Link } from 'react-router-dom'


class Forget extends Component {
  constructor() {
    super();
    this.state = {

    }
    this.handleClick = this.handleClick.bind(this);
    this.showPass = this.showPass.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSwitch = this.handleSwitch.bind(this);
  }
  handleSwitch(){
    this.setState(state => ({
      switch_otp_pass: !state.switch_otp_pass
    }));

  }
  handleChange(event){
    this.setState({[event.target.name] : event.target.value})
    if(event.target.value === ""){
      this.setState({[event.target.name+"_error"]:"please fill the Value"})
    }else{
      this.setState({[event.target.name+"_error"]:""}) 
    }
  }
  handleClick() {
    this.setState(state => ({
      showPass: !state.showPass,
      switch_uesr_pass: !state.switch_uesr_pass
    }));
  }
  showPass() {
    this.setState(state => ({
      showPass: !state.showPass
    }));
  }

  render() {
    return (
      <div>
        <div className="forget">
          <div className="login-page dark login-page-background">
            <div className="scroll-login">
              <div className="login-card">
                <div className="login-card_section">
                  <div className="card-body-section-center">
                    <div className="center-flex">
                      <div className="login-card-header no-pad">
                        <div>
                          <h2>Forget  </h2>
                          <small>Please Click any one below to Recover your account </small>
                        </div>
                      </div>
                      <div className=" no-pad">
                        <form autoComplete="off">
                          <div
                            className={
                              this.state.switch_uesr_pass
                                ? "forget-form step1 sections-step"
                                : "forget-form step1 sections-step active"
                            }
                          >
                              <Link to="/forgetsms" className="selection button">
                                  <div className="icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 512 512" >
<g>
	<g>
		<path d="M349.867,0H162.133c-23.564,0-42.667,19.103-42.667,42.667v426.667c0,23.564,19.103,42.667,42.667,42.667h187.733
			c23.564,0,42.667-19.102,42.667-42.667V42.667C392.533,19.103,373.431,0,349.867,0z M375.467,469.333
			c0,14.138-11.461,25.6-25.6,25.6H162.133c-14.138,0-25.6-11.461-25.6-25.6v-93.867h238.933V469.333z M375.467,358.4H136.533v-256
			h238.933V358.4z M375.467,85.333H136.533V42.667c0-14.138,11.462-25.6,25.6-25.6h187.733c14.138,0,25.6,11.462,25.6,25.6V85.333z"
			/>
	</g>
</g>
<g>
	<g>
		<path d="M256,392.533c-23.564,0-42.667,19.103-42.667,42.667s19.102,42.667,42.667,42.667s42.667-19.103,42.667-42.667
			S279.564,392.533,256,392.533z M256,460.8c-14.138,0-25.6-11.462-25.6-25.6s11.461-25.6,25.6-25.6s25.6,11.461,25.6,25.6
			S270.138,460.8,256,460.8z"/>
	</g>
</g>
<g>
	<g>
		<path d="M281.6,42.667h-51.2c-4.713,0-8.533,3.82-8.533,8.533s3.82,8.533,8.533,8.533h51.2c4.713,0,8.533-3.82,8.533-8.533
			S286.313,42.667,281.6,42.667z"/>
	</g>
</g>
</svg>
</div>
                                  <div className="text"><div className="selection_title">Via Sms</div><div className="selection_info">+91 ***** ***33</div></div>

                              </Link>
                              <Link to="/forgetemail" className="selection button">
                                  <div className="icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
	 viewBox="0 0 512 512" ><g><g><path d="M469.333,64H42.667C19.135,64,0,83.135,0,106.667v298.667C0,428.865,19.135,448,42.667,448h426.667
			C492.865,448,512,428.865,512,405.333V106.667C512,83.135,492.865,64,469.333,64z M42.667,85.333h426.667
			c1.572,0,2.957,0.573,4.432,0.897c-36.939,33.807-159.423,145.859-202.286,184.478c-3.354,3.021-8.76,6.625-15.479,6.625
			s-12.125-3.604-15.49-6.635C197.652,232.085,75.161,120.027,38.228,86.232C39.706,85.908,41.094,85.333,42.667,85.333z
			 M21.333,405.333V106.667c0-2.09,0.63-3.986,1.194-5.896c28.272,25.876,113.736,104.06,169.152,154.453
			C136.443,302.671,50.957,383.719,22.46,410.893C21.957,409.079,21.333,407.305,21.333,405.333z M469.333,426.667H42.667
			c-1.704,0-3.219-0.594-4.81-0.974c29.447-28.072,115.477-109.586,169.742-156.009c7.074,6.417,13.536,12.268,18.63,16.858
			c8.792,7.938,19.083,12.125,29.771,12.125s20.979-4.188,29.76-12.115c5.096-4.592,11.563-10.448,18.641-16.868
			c54.268,46.418,140.286,127.926,169.742,156.009C472.552,426.073,471.039,426.667,469.333,426.667z M490.667,405.333
			c0,1.971-0.624,3.746-1.126,5.56c-28.508-27.188-113.984-108.227-169.219-155.668c55.418-50.393,140.869-128.57,169.151-154.456
			c0.564,1.91,1.194,3.807,1.194,5.897V405.333z"/></g></g>
</svg></div>
                                  <div className="text"><div className="selection_title">Via Mail</div><div className="selection_info">*********@gmail.com</div></div>

                              </Link>
                              <div className="row">
                              <div className="col-md-12 col-xs-12 text-left">
                                <Link className="link forgetpassword-btn" to="/login">
                                  Sign In 
                                </Link>
                              </div></div><div className="clear"></div></div>
                           
                            <div className="row no-mar">
                             

                              <div className="col-md-12 col-xs-12 text-left mt-10">
                                <span>Do not have an account? </span>
                                <Link className="link signup-btn" to="/signup"> Sign Up</Link>
                              </div>
                            </div>
                       
                         
                        </form>
                        
                      </div>
                      <div className="login-card-footer"></div>
                    </div>
                  </div>
                </div>
               

                <div className="footer-mobile">
                  Licensed By &#169; aalavai.com . All rights reserved.
                </div>
              </div>
            </div>{" "}
            <div className="footer">
              Licensed By &#169; aalavai.com . All rights reserved.
            </div>{" "}
          </div>
        </div>{" "}
      </div>
    );
  }
}

export default Forget;
